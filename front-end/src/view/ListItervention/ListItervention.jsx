import { InterventionItem } from "../../components/InterventionItem/interventionItem";
import { Header } from "../../components/header/header";
import { PlaqueImmatriculation } from "../../components/plaqueImatriculation/plaqueImmatriculation.jsx";
import { getItervention } from "../../service/inteventionService";
import "./listIntervention.scss"
export function ListItervention() {
    const data = getItervention();

    return (
        <div className="container">
            <Header />
            <PlaqueImmatriculation numberPlaque={"XX-XXX-XX"} />
            {data && data.map((value) => (<InterventionItem key={value.id} isEnable={value.isEnable} textStatus={value.textStatus} numberPhone={value.numberPhone} address={value.address} ville={value.ville} />))}
        </div>
    )
}
