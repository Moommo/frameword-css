import camion from '../../assets/svg/Camion.svg'
import "./plaqueImmatriculation.scss"

export function PlaqueImmatriculation({ numberPlaque }) {
    return (
        <div className={"plaqueImmatriculation"}>
            <div className={"plaqueImmatriculation__left"}>
                <img src={camion} alt="un camion blanc" />
            </div>
            <div className={"plaqueImmatriculation__right"}>
                <p>{numberPlaque}</p>
            </div>
        </div>
    )
}

